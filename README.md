<!-- <a href="https://www.linkedin.com/in/yaroslava-yates-629517221/">
<img src="https://i.postimg.cc/x18crzfP/linkedin-logo.webp" alt="linkedin" hspace="50" height="22" width="82" align="right"></a> -->

<a href="https://www.linkedin.com/in/yaroslava-yates-629517221/">
  <img src="https://img.shields.io/static/v1?label=Linked&message=In&color=286fc7" alt="LinkedIn" hspace="5" height="20" width="80" align="right"></a>

<a href="https://www.codewars.com/users/amfibiya17" > ![CodeWars](https://www.codewars.com/users/amfibiya17/badges/small) </a>

<h1 align="center">
  Yaroslava Yates
</h1>



<div align="center">

  <a href="https://github.com/anuraghazra/github-readme-stats">
    <img align="center" src="https://github-readme-stats.vercel.app/api?username=amfibiya17&theme=default" height="163"/>
  </a>

  <a href="https://github.com/anuraghazra/github-readme-stats">
    <img align="center" src="https://github-readme-stats.vercel.app/api/top-langs/?username=amfibiya17&layout=compact&theme=default" height="163"/>
  </a>

</div>

<br>
<br>

<!--
<div align="center">

  [Projects ](#projects) |
  [Skills ](#skills) |
  [Education ](#education) |
  [Experience ](#experience) |
  [Other Projects ](#other-projects) |
  [Interests ](#interests)

</div>

-->


<div align="center">

  [![Projects](https://img.shields.io/badge/-Projects-B21807)](#projects) | 
  [![Skills](https://img.shields.io/badge/-Skills-orange)](#skills) | 
  [![Education](https://img.shields.io/badge/-Education-yellow)](#education) | 
  [![Experience](https://img.shields.io/badge/-Experience-green)](#experience) | 
  [![Other Projects](https://img.shields.io/badge/-Other%20Projects-blue)](#other-projects) | 
  [![Interests](https://img.shields.io/badge/-Interests-purple)](#interests)

</div>

---

I am a Software Engineer and recent graduate of the highly selective [Makers Academy](https://makers.tech/) programming bootcamp.

I have a Bachelor’s degree in Mathematics with Management and have worked in finance, customer services and logistics over the past 6 years.

I started coding in March 2020 (during the first lockdown), completed some challenges and workshops, wrote small programs and fell in love with coding.
I realised that I love solving problems and, as a Software Engineer, I hope to be able to constantly provide solutions to users’ problems.
Working in the tech industry means I will forever learn new things: technology is extremely fast moving, so Software Engineers need to be able to learn new technologies and adapt to the continually changing environment.
The satisfaction from writing code is unmatched. Unlike some other jobs, my results are always visible after I design and deploy something in code.

As a graduate of Makers Academy, I now have the skills to design, test, build and deploy full stack applications using JavaScript and Ruby. I am trained in OOP, TDD, pair programming and agile processes.

In non-code life (sounds so unrealistic now), I had the opportunity to change countries twice - moving from my motherland Ukraine to Russia when I was 10, and then to the UK 19 years later; to learn new languages - Ukrainian, Russian and English (and basic French); to start everything from scratch here in the UK. I now understand that I made the right decision to change career and am excited to bring my passion and enthusiasm to a new role.

---
<br>

## Projects

<br>

| Project<br>Demo Link | Description | Stack |
| ------------- | ------------- | ------------- |
| [palendar](https://thepalendar.netlify.app/) | MERN stack app with Weather API, which allows users to schedule a time to hang out with friends ([repo](https://github.com/amfibiya17/final-project)) | JS, CSS, HTML, MongoDB, React, NodeJS, Jest, Bcrypt, Cypress, Nodemone, Mongoose |
| [weather web app](http://weather--app.s3-website.eu-west-2.amazonaws.com/) | API Weather Web App Frond End project with AJAX deployed to AWS S3 ([repo](https://github.com/amfibiya17/weather-app)) | Vanilla JS, CSS, HTML, Jest, NodeJs |
| [noughts & crosses game](http://noughts-and-crosses.s3-website.eu-west-2.amazonaws.com/) | Classic Noughts and Crosses Game for 2 players with score counter deployed to AWS S3 ([repo](https://github.com/amfibiya17/noughts-and-crosses)) | React, JS, CSS, HTML, Jest, Enzyme |
| [news app](http://news-app-challenge.s3-website.eu-west-2.amazonaws.com/) | API News app Frond End challenge with AJAX deployed to AWS S3 ([repo](https://github.com/amfibiya17/news-summary-challenge)) | Vanilla JS, CSS, HTML |
| [chitter app](https://chitter---app.herokuapp.com/)   | Basic Twitter clone with PostgreSQL database deployed to Heroku ([repo](https://github.com/amfibiya17/chitter-challenge)) | Ruby, CSS, HTML, Sinatra, PostgreSQL, RSpec, Capybara |


---
<br>


## Skills

<br>

### Fast learner
The Makers-centric methodology focuses on TDD, DDD and Agile development. It added great value to my process and code testing practice. By the end of the course, I was able to contribute to a culture of continuous improvement, self-managing teams and successful team delivery via our Agile practices and processes. For example:
- strictly following Agile with continuous deployment as part of this approach 
- using git, which allowed branching out from the original code base and isolating work in progress from the main branch
- using different types of testing (unit, end-to-end and React testing library) to ensure changes to code behaved as expected

<br>

During Makers Bootcamp, I started thinking about challenging myself with a third language.  
I found that C# is one of the most popular programming languages.
It can be used for a variety of things, including mobile applications, game development, and enterprise software.  
I have completed “Learn C# for beginners” on “Codecademy” and think that this language is one of the most well-designed programming languages.
Unlike languages like Ruby and JavaScript, which I learned at Makers, C# defines the type of data in a program. Assigning a type essentially tells a computer what operations can and cannot be performed on that piece of data.  
I used this knowledge to build a simple C#/.NET application by strictly following TDD process -   
please see my latest projects [Cloud Customers](https://github.com/amfibiya17/c-sharp-practice) and [C# Tiny Projects](https://github.com/amfibiya17/c-sharp-tiny-projects).

<br>

### Problem-solver
During my tech journey I learned:
- how to split large complex tasks into small, simpler ones
- how to think parallel: when tasks can be done one at a time, sequentially, in a particular order
- how to consider re-using existing solutions: using tools that are already available before I start designing a solution from scratch
- how to think in terms of Data Flows: visualising the primary task and its subtasks

Please see examples of approaches: [gilded rose tech test](https://github.com/amfibiya17/gilded-rose-tech-test) and [bank tech test](https://github.com/amfibiya17/bank-tech-test)

<br>

### SDLC follower
During my Makers journey I learned, understood and used the full engineering life cycle, from design and implementation through to testing and deployment. For example in one of my weekend challenges (Chitter Challenge) I used these steps: 
- plan the timetable with target goals, define requirements to determine what the application is supposed to do
- design phase models the way the application will work
- testing, strictly following TDD and BDD practices
- coding process, setting up database
- documentation, create a README file, which includes instructions and explanations
- deploy (in my case to Heroku) and maintain the app

<br>

### Deployment lover
Throughout my time at Makers, I really enjoyed deploying my projects to platforms such as Heroku and AWS S3. I started deploying my projects from the the very beginning: the ability to see the final product I am building and delivering has highlighted the importance of contributing to a mature DevOps culture and a cloud-native technical architecture that is modular, adaptable and scalable.

---

<br>


## Education

### Makers Academy - Software Engineering Bootcamp (July 2022)
  - Paradigms: OOP, TDD, MVC, DDD, SVC (Git)
  - Methodology: Agile/XP
  - Languages: Ruby, JavaScript, Node
  - Frameworks: Rails, Sinatra, React, Express
  - Testing libraries: RSpec, Jasmine, Jest, Enzyme, React testing Library, Cypress, Capybara
  - Databases: MongoDB, PostgreSQL
  - Cloud platforms: Heroku, MongoDB Atlas, AWS S3
  - Team collaboration

<br>

### Tyumen State University, Russia (June 2011)
 - Bachelor's degree in Mathematics with Management, 2:1

<br>

### Nizhnevartovsk Grammar School, Russia (June 2003)
   - AS-Level equivalent in Russian language (Grade A), Mathematics (Grade B) and History (Grade A)

<br>

### Nizhnevartovsk Grammar School, Russia (June 2001)
  - GCSE equivalent in 19 subjects including Mathematics, English, Physics, Chemistry and Biology  
  at Grades A - C

<br>

### Other Certifications and Courses

  - AWS - Cloud Practitioner Certification (pending)
  - Codecademy - Learn C# Course (Jul 2022)
  - Codecademy - Learn Ruby Course (Jan 2022)
  - Codecademy - Learn CSS Course (Jan 2022)
  - Codecademy - Learn JS Course (Jan 2022)
  - Cambridge Regional College – Web Development with WordPress Workshop (Jan. 2022)
  - SheCodes Workshop - Introduction to Coding (Aug 2020)
  - Codecademy - Learn HTML Course (Jul 2020)



---

<br>

## Experience

### Operations Supervisor - Breezemount (IKEA)
#### (Sep 2021 – Feb 2022)
  - Supervising activities of administrators, warehouse and delivery crews
  - Ensuring all daily operational activities are completed in line with Company procedures
  - Ensuring cost effective and compliant route and resource planning, delivery crew debrief and updating records and systems as appropriate
  - Proactive customer contact and accurate processing of failed deliveries to include rescheduling and stock returns to client
  - Was promoted within weeks of starting from admin to supervisor position

<br>

### Travel Money Advisor - Sainsbury’s Bank
#### (Sept 2018 – Sept 2021)
  - Buying and selling foreign currencies, selling, reloading, and cashing out travel money cards, sending and paying out transactions via Western Union
  - Ordering and packing online and “walk-up” orders, organising and ordering delivery and pick-up of pounds and foreign currencies
  - Writing up monthly reports for duty, area and regional managers, drawing up monthly rotas for the bureau, training colleagues
  - “Bureau of the Month” award for June 2019

<br>

### Contact Tracing Tier 3 Customer Services Advisor - Sitel, NHS COVID-19 Test and Trace Programme
#### (May 2020 – January 2021)
  - Interviewing contacts of COVID-19 cases in line with the contact tracing and management protocol and further protocols or guidance as indicated
  - Escalating and referring challenging or complex cases, incidents, and outbreaks of COVID-19 to Tier 2 Call Handlers or the Tier 3 Call Handler Team Lead as appropriate
  - Ensuring the timely and accurate recording of information from interviews, contributing to the maintenance of effective systems for the surveillance of COVID-19 through providing accurate data and information
  - Maintaining confidentiality in relation to personal data held in accordance with the Data Protection Act and Caldicott Guardianship principles



---

<br>

## Other Projects

| Project<br>Demo Link | Description | Stack |
| ------------- | ------------- | ------------- |
| [todo list](https://todo--list--challenge.herokuapp.com/) | Simple To Do list with AJAX  deployed to Heroku ([repo](https://github.com/amfibiya17/notes-app)) | JS, CSS, HTML |
| [caterpillar game](https://caterpillar-game.herokuapp.com/)  | Popular snake/caterpillar game from the 90's deployed to Heroku ([repo](https://github.com/amfibiya17/caterpillar-game)) | JS, CSS, HTML, PHP |
| [rps-game](https://rock---paper---scissors.herokuapp.com/)  | Popular rock-paper-scissors game for one player vs. computer deployed to Heroku ([repo](https://github.com/amfibiya17/rps-challenge)) | Ruby, CSS, HTML, Sinatra, RSpec, Capybara
| [birthday-app](https://birth-day-app.herokuapp.com/)  | Simple web application, showing how many days till your next Birthday deployed to Heroku ([repo](https://github.com/amfibiya17/Birthday-App)) | Ruby, CSS, HTML, Sinatra, RSpec, Capybara
| [website challenge](https://shecodesproject2020.s3.eu-west-2.amazonaws.com/new_site.html) | First website challenge as part of SheCodes bootcamp deployed to AWS S3 | CSS, HTML, JS |


---

<br>

## Interests
  - Piano player
  - Avid reader
  - Coin collector
  - Dog lover
  - Adventurer
